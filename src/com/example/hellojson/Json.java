package com.example.hellojson;

import java.io.IOException;
import java.io.InputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Bundle;

public class Json extends ActionBarActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_json);
		//获取json文件
		InputStream is = this.getResources().openRawResource(R.raw.json);

		try {
			//获取json文件中的有效内容
			byte[] buffer = new byte[is.available()];
			is.read(buffer);
			
			//将字节数组转换为以GB2312编码的字符串
			String json = new String(buffer,"GB2312");
			
			//将字符串json转换为json对象，以便于取出数据
			JSONObject jsonobject = new JSONObject(json);
			
			//获取name
			String name = jsonobject.getString("姓名");
			//获取sex
			String sex = jsonobject.getString("性别");
			//获取age
			String age = jsonobject.getString("年龄");		
			
			//大括号括起来的内容就表示一个JSONObject，所以这里要再创建一个JSONObject对象
			JSONObject chengji = jsonobject.getJSONObject("学习成绩");
			
			//获取语文的成绩
			String yuwen = chengji.getString("语文");
			//获取数学的成绩
			String shuxue = chengji.getString("数学");
			//获取英文的成绩
			String yingwen = chengji.getString("英文");
			
			//大括号括起来的内容就表示一个JSONObject，所以这里要再创建一个JSONObject对象
			JSONArray zh = chengji.getJSONArray("综合");
			//获取文科综合的成绩
			JSONObject wz = zh.getJSONObject(0);
			String wenzonghe = wz.getString("文科综合");
			//获取理科综合的成绩
			JSONObject lz = zh.getJSONObject(1);
			String lizonghe = lz.getString("理科综合");			
			
			//显示value的值
			Toast.makeText(getApplicationContext(), "姓名:		"+name+
													"\n性别:		"+sex+
													"\n年龄:		"+age+
													"\n语文:		"+yuwen+
													"\n数学:		"+shuxue+
													"\n英文:		"+yingwen+
													"\n文科综合:	"+wenzonghe+
													"\n理科综合:	"+lizonghe, Toast.LENGTH_LONG).show();
			
			TextView tv = (TextView)super.findViewById(R.id.textview1);
			
			tv.setTextColor(0xFFFF0000);
			tv.setTextSize(30);
			tv.setText("姓名: "+name+
					   "\n性别: "+sex+
					   "\n年龄: "+age+
					   "\n语文: "+yuwen+
					   "\n数学: "+shuxue+
					   "\n英文 : "+yingwen+
					   "\n文科综合: "+wenzonghe+
					   "\n理科综合: "+lizonghe);
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
